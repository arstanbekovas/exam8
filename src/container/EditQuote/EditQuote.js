import React, {Component} from 'react';
import axios from 'axios';
import './EditQuote.css'

class EditQuote extends Component {
    state = {
        quotes:  {
            author: '',
            text: '',
            category: 'star-wars'
        },
        categories: [
            {title: 'Star Wars', id: 'star-wars'},
            {title: 'Famous people', id: 'famous-people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humour', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        loading: false
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/quotes/${id}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedQuote: response.data, quotes: response.data})

        })
    }
    saveQuoteHandler = e => {
        e.preventDefault();

        this.setState({loading: true});
        const id = this.props.match.params.id;

        axios.patch(`/quotes/${id}.json`, this.state.quotes).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };

    quoteValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quotes: {...prevState.quotes, [e.target.name]: e.target.value}
            }
        });
    };


    render(){
        return(
            <form className="EditQuote">
                <h1>Edit a quote</h1>
                <select className='dropdown' name="category" value={this.state.quotes.category} onChange={this.quoteValueChanged}>
                    {this.state.categories.map(category =>
                        <option value={category.id}>
                            {category.title}
                        </option>
                    )}
                </select>
                <input type="text" name="author" placeholder="Author" value={this.state.quotes.author} onChange={this.quoteValueChanged}/>
                <textarea name="text" placeholder="Quote text" value={this.state.quotes.text} onChange={this.quoteValueChanged}>
                </textarea>
                <button onClick={this.saveQuoteHandler}>Save</button>
            </form>
        )
    }
}
export default EditQuote;