import React, {Component, Fragment} from 'react';
import Quote from "../../components/Quote/Quote";
import axios from 'axios';
import {NavLink} from "react-router-dom";
import './QuotesList.css';

class QuotesList extends Component {
    state = {
        categories: [
            {title: 'Star Wars', id: 'star-wars'},
            {title: 'Famous people', id: 'famous-people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humour', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        quotes: [],
        selectedCategoryId: null
    };
    loadData = () => {
        console.log(this.props.match.params.category);
        let url = '/quotes.json';
        if (this.props.match.params.category) {
            url = '/quotes.json?orderBy="category"&equalTo="' + this.props.match.params.category + '"';
        }
        axios.get(url).then((response) => {
            console.log(response);
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key})
            }
            this.setState({quotes, selectedCategoryId: this.props.match.params.category})
        })
    };

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate() {
        console.log('updated:', this.state.selectedCategoryId, this.props.match.params.category);
        if (this.state.selectedCategoryId !== this.props.match.params.category) {
            console.log('here');
            this.loadData();
        }
    }

    deleteQuoteHandler = id => {
        axios.delete(`/quotes/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes};
            })
        })
    };

    render() {

        return (
            <Fragment>
                <h2>Quotes central</h2>
                <h3 className='categoryName'>{this.props.match.params.category}</h3>
                <section><li className="QuoteLi"><NavLink to={'/'}>All</NavLink></li></section>
                <section className='CategoriesList'>
                    {this.state.categories.map(category =>
                        <li className="QuoteLi">
                            <NavLink
                                to={'/' + category.id}
                                key={category.id}>
                                {category.title}
                            </NavLink>
                        </li>
                    )}
                </section>

                <section className='QuoteList'>
                    {this.state.quotes.map(quote => (
                        <Quote
                            key={quote.id}
                            author={quote.author}
                            id={quote.id}
                            text={quote.text}
                            category={quote.category}
                            deleted={() => this.deleteQuoteHandler(quote.id)}
                        />

                    ))}
                </section>
            </Fragment>
        )
    }
}

export default QuotesList;