import React, {Component} from 'react';
import './AddQuote.css'
import axios from 'axios';
import 'react-dropdown/style.css'



class  AddQuote extends Component {
    state = {
        quotes:  {
            author: '',
            text: '',
            category: 'star-wars'
        },
        categories: [
            {title: 'Star Wars', id: 'star-wars'},
            {title: 'Famous people', id: 'famous-people'},
            {title: 'Saying', id: 'saying'},
            {title: 'Humour', id: 'humor'},
            {title: 'Motivational', id: 'motivational'}
        ],
        loading: false
    };


    saveQuoteHandler = e => {
        e.preventDefault();

        this.setState({loading: true});

        axios.post('/quotes.json', this.state.quotes).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };

    quoteValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                quotes: {...prevState.quotes, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <form className="AddQuote">
                <h1>Submit New quote</h1>
                <select name="category" value={this.state.quotes.category} onChange={this.quoteValueChanged}>
                    {this.state.categories.map(category =>
                        <option key={category.id} value={category.id}>
                            {category.title}
                        </option>

                    )}
                </select>
                <input type="text" name="author" placeholder="Author" value={this.state.quotes.author} onChange={this.quoteValueChanged}/>
                <textarea name="text" placeholder="Quote text" value={this.state.quotes.text} onChange={this.quoteValueChanged}>
                </textarea>
                <button onClick={this.saveQuoteHandler}>Save</button>
            </form>
        )
    }

};
export default AddQuote;