import React, { Component, Fragment } from 'react';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import QuotesList from "./container/QuotesList/QuotesList";
import AddQuote from "./container/AddQuote/AddQuote";
import EditQuote from "./container/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
        <div className='App'>
            <Fragment>
                <nav className="App-nav">
                    <ul>
                        <li className ='App-li'><NavLink to="/" exact>Quotes</NavLink></li>
                        <li className ='App-li'><NavLink to="/add" exact >Submit new quote</NavLink></li>
                    </ul>
                </nav>

                <Switch>
                    <Route path="/" exact component={QuotesList}/>
                    <Route path="/add" component={AddQuote}/>
                    <Route path='/quotes/:id/edit' exact component={EditQuote} />
                    <Route path='/:category' exact component={QuotesList} />
                    <Route render={() =>  <h1>Not Found</h1>}/>
                </Switch>
            </Fragment>
        </div>

    );
  }
}

export default App;
