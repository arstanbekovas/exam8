import React from 'react';
import {NavLink} from "react-router-dom";
import './Quote.css'


const Quote = props => {
        return(
            <div className="Quote">
                <p>Quote: "{props.text}"</p>
                <p>***author: {props.author}</p>
                <a className='button' href="#" onClick={props.deleted}>Delete</a>
                <NavLink className='button' to={'/quotes/' + props.id + '/edit'}>Edit</NavLink>
            </div>
        )
};

export default Quote;